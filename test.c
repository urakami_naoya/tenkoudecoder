
/*
 * File:   test.c
 * Author: urakaminaoya
 *
 * Created on April 14, 2018, 7:26 PM
 */

// CONFIG
#pragma config FOSC = HS        // Oscillator Selection bits (HS oscillator)
#pragma config WDTE = OFF       // Watchdog Timer Enable bit (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable bit (PWRT disabled)
#pragma config CP = OFF         // FLASH Program Memory Code Protection bits (Code protection off)
#pragma config BOREN = OFF      // Brown-out Reset Enable bit (BOR disabled)
#pragma config LVP = OFF        // Low Voltage In-Circuit Serial Programming Enable bit (RB3 is digital I/O, HV on MCLR must be used for programming)
#pragma config CPD = OFF        // Data EE Memory Code Protection (Code Protection off)
#pragma config WRT = OFF        // FLASH Program Memory Write Enable (Unprotected program memory may not be written to by EECON control)

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#define _XTAL_FREQ     20000000

#include <xc.h>
#include "decoder.h"

int test_decoder_1(void) {
    decoder_initialize();
    for(;;) {
        for(uint8_t q_pin = 0 ; q_pin < 8 ; q_pin++) { //EX) q_pin = 0 mean Q0 is LOW 
            decoder_select_pin(q_pin);
            __delay_ms(10);
        }
        __delay_ms(5);
    }
}

int main(void){
    int a = test_decoder_1();
}
