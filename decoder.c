#include "decoder.h"

/**
 * decoder_initialize
 * 
 * To set three pins for decoder to output
 * 
 * First need to use this functions in Gyro and Magnetmetor
 */

void decoder_initialize(void) {
    SPI_SENSE_CS_ADS_CS_DEV1_INIT = 0;
    SPI_SENSE_CS_ADS_CS_DEV2_INIT = 0;
    SPI_SENSE_CS_ADS_CS_DEV3_INIT = 0;
}

void decoder_select_pin(uint8_t q_output) {
    SPI_SENSE_CS_ADS_CS_DEV1 = q_output & 0x01 ? 1 : 0;
    q_output >>= 1;
    SPI_SENSE_CS_ADS_CS_DEV2 = q_output & 0x01 ? 1 : 0;
    q_output >>= 1;
    SPI_SENSE_CS_ADS_CS_DEV3 = q_output & 0x01 ? 1 : 0;
}

void decoder_select_slave(uint8_t q_out , uint8_t clock_polarity) {
    decoder_select_pin(7);
    SPI_CK_MASTER = clock_polarity & 0x01 ? 1 : 0;
    decoder_select_pin(q_out);
}
