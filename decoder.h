/* 
 * File:  TenkouDecoder 
 * Author: Urakami
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef DECODER_H
#define	DECODER_H

#include "TK-02-02-01_IF_PV.h"
#include "spi_pins.h"

void decoder_initialize(void);
void decoder_select_pin(uint8_t q_output);
void decoder_select_slave(uint8_t q_out , uint8_t clock_polarity);

#endif	/* XC_HEADER_TEMPLATE_H */

